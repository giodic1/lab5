package edu.towson.cosc435.valis.labsapp.ui

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import edu.towson.cosc435.valis.labsapp.model.Song

class SongsListViewModel : ViewModel() {
    private var original = (0..20).map { i ->
        Song("Song $i", "Artist $i", i, i % 3 == 0)
    }

    private val _songs: MutableState<List<Song>> = mutableStateOf(original)
    val songs: State<List<Song>> = _songs

    private val _currentSong: MutableState<Song> = mutableStateOf(_songs.value.get(0))
    val currentSong: State<Song> = _currentSong

    fun setCurrentSong(song: Song) {
        _currentSong.value = song
    }

    fun onDelete(idx: Int) {
        _songs.value = _songs.value.subList(0, idx) + _songs.value.subList(idx+1, _songs.value.size)
        original = _songs.value
    }

    fun onToggle(idx: Int) {
        _songs.value = songs.value.mapIndexed { index, song ->
            if(idx == index) {
                song.copy(isAwesome = !song.isAwesome)
            } else
                song
        }
        original = _songs.value
    }

    fun onFilter(s: String) {
        _songs.value = original.filter { a -> a.name.contains(s, true) }
    }

    fun addSong() {

    }
}