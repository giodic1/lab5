package edu.towson.cosc435.valis.labsapp.ui

import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable

@Composable
fun ConfirmDialog(
    title: String,
    text: String,
//    confirmViewModel: IConfirmViewModel
) {
    // TODO - 7a. Create a State variable for showing/hiding the dialog
    if(false) {
        AlertDialog(
            onDismissRequest = { // TODO - 7c. Call the dismiss function
            },
            title = { Text(title) },
            text = { Text(text) },
            confirmButton = {
                Button({
                    // TODO - 7b. Call the confirmation function
                }) {
                    Text("Delete")
                }
            }
        )
    }
}