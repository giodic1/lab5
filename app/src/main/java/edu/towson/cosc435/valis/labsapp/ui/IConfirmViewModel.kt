package edu.towson.cosc435.valis.labsapp.ui

import androidx.compose.runtime.State

interface IConfirmViewModel {
    val showConfirmDialog: State<Boolean>
    fun showConfirmDelete(onConfirm: () -> Unit)
    fun onConfirmDelete()
    fun dismissDialog()
}