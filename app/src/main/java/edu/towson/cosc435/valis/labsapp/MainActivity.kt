package edu.towson.cosc435.valis.labsapp

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import edu.towson.cosc435.valis.labsapp.model.Album
import edu.towson.cosc435.valis.labsapp.model.Song
import edu.towson.cosc435.valis.labsapp.ui.*
import edu.towson.cosc435.valis.labsapp.ui.theme.LabsAppTheme

class MainActivity : ComponentActivity() {

    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Log.d("TAG", "onCreate")
            LabsAppTheme {
                val songsListViewModel: SongsListViewModel by viewModels()
                Surface(color = MaterialTheme.colors.background) {
                    MainScreen(
                        songs = songsListViewModel.songs.value,
                        currentSong = songsListViewModel.currentSong.value,
                        onDelete=songsListViewModel::onDelete,
                        onToggle=songsListViewModel::onToggle,
                        onFilter=songsListViewModel::onFilter,
                        onAddSong = songsListViewModel::addSong
                    )
                }
            }
        }
    }
}

@ExperimentalFoundationApi
@Composable
fun MainScreen(
    songs: List<Song>,
    currentSong: Song,
    onDelete: (Int) -> Unit,
    onToggle: (Int) -> Unit,
    onFilter: (String) -> Unit,
    onAddSong: () -> Unit, // TODO - Pass in a song?
) {
    ConfirmDialog(
        title = "Confirm",
        text = "Are you sure you want to delete?"
    )
    // TODO - 6. When in landscape, display the LandscapeView composable
    val config = LocalConfiguration.current
    if(config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
        LandscapeView(song = currentSong) {
            LazyColumn {
                itemsIndexed(songs) { idx, song ->
                    SongRow(idx, song, onDelete, onToggle)
                }
            }
        }
    } else {
        Column() {
            SearchBar(onFilter=onFilter)
            AddSongFAB(onClick = onAddSong)
            LazyColumn {
                itemsIndexed(songs) { idx, song ->
                    SongRow(idx, song, onDelete, onToggle)
                }
            }
        }
    }
}

// TODO - 2. Implement the ISongsRepository interface
// TODO - 7. (Optional) Implement a "Confirm deletion" dialog box

@ExperimentalFoundationApi
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    val songs = (0..20).map { i ->
        Song("Song $i", "Artist $i", i, i % 3 == 0)
    }
    LabsAppTheme {
        Surface(color = MaterialTheme.colors.background) {
            MainScreen(songs, songs.get(0), {}, {}, {}, {})
        }
    }
}